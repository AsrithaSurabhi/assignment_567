
import java.util.ArrayList;
import java.util.Date;

public class Main {
	public static void main(String[] args) {
		
		
		CustomerInfo john = new CustomerInfo(1, "John", "Lee", "john@gmail.com");

		Bills b1 = new Hydrobill(1, new Date(), "Hydro", 35.78, "HydroOne", 20);
		Bills b2 = new InternetInfo(2, new Date(), "Internet", 40.70, "Freedom", 250);

		ArrayList<Bills> bills = new ArrayList<>();
		bills.add(b2);
		bills.add(b1);
		john.setBills(bills);

		CustomerInfo kevin = new CustomerInfo(2, "kevin", "paul", "kevin@gmail.com");

		Bills b3 = new Hydrobill(1, new Date(), "Hydro", 40.26, "Enbridge", 23);
		Bills b4 = new InternetInfo(2, new Date(), "Internet", 45.60, "Telus", 300);
		Bills b5 = new Mobile(3, new Date(), "Mobile", 250.69, "Samsung", "Prepaid Talk + Unlimited Text plan",
				437852963, 2, 260);
		Bills b6 = new Mobile(4, new Date(), "Mobile", 405.78, "One Plus", "LTE+4G 10GB Data Promo plan",
				437854589, 6, 150);

		ArrayList<Bills> bills1 = new ArrayList<>();
		bills1.add(b6);
		bills1.add(b4);
		bills1.add(b5);
		bills1.add(b3);

		kevin.setBills(bills1);

		CustomerInfo peter = new CustomerInfo(3, "peter", "parker", "peter@gmail.com");

		CustomerStorage cs = new CustomerStorage();

		cs.addCustomer(peter);
		cs.addCustomer(john);
		cs.addCustomer(kevin);

		cs.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************ 	            Searching a customer      	         ***********");
		System.out.println("***********************************************************************");
		
		
		// Search customer
		CustomerInfo c1 = cs.getCustomerById(1);
		c1.display();

		System.out.println("\n\n\n***********************************************************************");
		System.out.println("************    Searching a customer that doesn't exists     ***********");
		System.out.println("***********************************************************************");

		cs.getCustomerById(10);
	}
}
