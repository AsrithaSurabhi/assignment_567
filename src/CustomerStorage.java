
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomerStorage implements Display {

	private ArrayList<CustomerInfo> customers = new ArrayList<>();

	public void addCustomer(CustomerInfo customer) {
		this.customers.add(customer);
	}

	public CustomerInfo getCustomerById(int id) {
		CustomerInfo c = null;
		for (CustomerInfo customer : customers) {
			if (customer.getCustomerId() == id) {
				c = customer;
			}
		}

		if (c == null) {
			System.out.println("No customer with id " + id + " exists.");

		}
		return c;
	}

	@Override
	public void display() {
		Collections.sort(customers, new CustomerCompare());
		for (CustomerInfo customer : customers) {
			customer.display();
		}
	}

}

class CustomerCompare implements Comparator<CustomerInfo> {

	@Override
	public int compare(CustomerInfo b1, CustomerInfo b2) {
		if (b1.getCustomerId() < b2.getCustomerId()) {
			return -1;
		} else if (b1.getCustomerId() > b2.getCustomerId()) {
			return 1;
		} else {
			return 0;
		}
	}

}